package com.jnew.draku.membank;
/**
 * Created by Michał on 2015-04-05.
 */
public interface IUserController {
    public void logIn();
    public void logOut();
    public User getUser();
    public void setUser(User user);
    public void saveNewUser();

}
