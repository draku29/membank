package com.jnew.draku.membank;

/**
 * Created by Michał on 2015-04-05.
 */
public interface IFileMarker {
     double getLat();
     void setLat(double lat);
     double getLng();
     void setLng(double lng);
     String getLinkToFile() ;
     void setLinkToFile(String linkToFile);
     boolean isDownloaded();
     void setDownloaded(boolean onDevice);
     String getPathOnDevice();
}
