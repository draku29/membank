package com.jnew.draku.membank;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.LogRecord;

/**
 * Created by Michał on 2015-04-06.
 */
public class SocketIOService extends IntentService {

    private Socket mSocket;
    /*{
        try {
            mSocket = IO.socket(Constants.SERVER_URL);
        } catch (URISyntaxException e) {}
    }*/
    //private static Socket socket;
    //private static SocketIOService instance;
    //static final int MSG_SAY_HELLO = 1;


    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
                emitMessage(message);
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());


    public SocketIOService(){
        super("SocketIOService");
    }
    /*public SocketIOService(String name){
        super(name);
    }*/

    @Override
    public void onCreate(/*Bundle savedInstanceState*/) {
        super.onCreate(/*savedInstanceState*/);
        Log.d("SocketIO.onCreate", "creating SocketIO");
        android.os.Debug.waitForDebugger();
        //try {
        //    mSocket = IO.socket(Constants.SERVER_URL);
        try {
            mSocket = IO.socket(Constants.SERVER_URL+Constants.SERVER_PORT);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, this.onConnectError);
            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.on(Socket.EVENT_DISCONNECT, onConnectError);
            mSocket.on(Socket.EVENT_CONNECT, onConnectCallback);
            mSocket.on("sign_up", onSignUpCallback);
            mSocket.on("log_in", onLogInCallback);
            mSocket.on("log_out", onLogOutCallback);
            mSocket.on("update_items_list", onUpdateItemsListCallback);
            mSocket.on("upload", onUploadCallback);
            mSocket.on("download", onDownloadCallback);

        //TODO(optional) forgot password
            Log.d("SocketIO.onCreate", "trying to connect");
            mSocket.connect();
        //} catch (URISyntaxException e) {}
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    final public void onDestroy(){
        //try {
            mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.off(Socket.EVENT_DISCONNECT, onConnectError);
            mSocket.off(Socket.EVENT_CONNECT, onConnectCallback);
            mSocket.off("sign_up", onSignUpCallback);
            mSocket.off("log_in", onLogInCallback);
            mSocket.off("log_out", onLogOutCallback);
            mSocket.off("update_items_list", onUpdateItemsListCallback);
            mSocket.off("upload", onUploadCallback);
            mSocket.off("download", onDownloadCallback);
        //}catch(Exception e){

        //}

        appendLog(DateFormat.getDateTimeInstance().format(new Date()) + ": Stopped", Constants.LOG_FILE);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(getApplicationContext(), "binding", Toast.LENGTH_SHORT).show();
        return mMessenger.getBinder();
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        emitMessage(intent);
    }


    public void appendLog(String text, String filename)
    {
        File logFile = new File(filename);
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
//BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private Emitter.Listener onConnectError = new Emitter.Listener(){
        @Override
        public void call(Object... args) {
            Log.d("Listener args", args[0].toString());
            /*JSONObject data = getJSONData(args);
            HashMap<String,String> messageData = new HashMap<>();
            try{
                messageData.put("status",data.getString("connectErrorStatus"));
            } catch(JSONException e){
                return;
                // TODO Proper catch
            }*/
            HashMap<String,String> messageData = new HashMap<>();
            messageData.put("status","connect error");

            sendMessage("connectError",messageData);
        }
        /*@Override
        public void call(Object.. args) {
                getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    String message;
                    try {
                        username = data.getString("username");
                        message = data.getString("message");
                    } catch (JSONException e) {
                        return;
                    }

                    // add the message to view
                    addMessage(username, message);
                }
            }); //TODO
        }*/
    };

    private Emitter.Listener onSignUpCallback = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = getJSONData(args);
            HashMap<String,String> messageData = new HashMap<>();
            try{
                messageData.put("status", data.getString("signUpStatus"));
            } catch(JSONException e){
                return;
                // TODO Proper catch
            }

            sendMessage("signUp",messageData);
        }
    };
    private Emitter.Listener onLogInCallback = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = getJSONData(args);
            HashMap<String,String> messageData = new HashMap<>();
            try{
                messageData.put("status",data.getString("logInStatus"));
            } catch(JSONException e){
                return;
                // TODO Proper catch
            }

            sendMessage("logIn",messageData);
        }
    };
    private Emitter.Listener onLogOutCallback = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = getJSONData(args);
            HashMap<String,String> messageData = new HashMap<>();
            try{
                messageData.put("status",data.getString("logOutStatus"));
            } catch(JSONException e){
                return;
                // TODO Proper catch
            }

            sendMessage("logOut",messageData);
        }
    };
    private Emitter.Listener onUpdateItemsListCallback = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = getJSONData(args);
            HashMap<String,String> messageData = new HashMap<>();
            try{
                messageData.put("LatLng", data.getString("LatLng"));
                messageData.put("filesList", data.getString("filesList")); //TODO Try array
            } catch(JSONException e){
                return;
                // TODO Proper catch
            }
            sendMessage("updateItemsList",messageData);
        }
    };
    private Emitter.Listener onUploadCallback = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = getJSONData(args);
            HashMap<String,String> messageData = new HashMap<>();
            try{
                messageData.put("uploadStatus", data.getString("uploadStatus"));
            } catch(JSONException e){
                return;
                // TODO Proper catch
            }

            sendMessage("uploadFile",messageData);
        }
    };
    private Emitter.Listener onDownloadCallback = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
                //TODO download file
                JSONObject data = getJSONData(args);
            HashMap<String,String> messageData = new HashMap<>();
            try{
                messageData.put("fileLink", data.getString("fileLink"));
            } catch(JSONException e){
                return;
                // TODO Proper catch
            }

            sendMessage("downloadFile",messageData);
        }
    };
    private Emitter.Listener onConnectCallback = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            HashMap<String,String> messageData = new HashMap<>();
            messageData.put("status","OK");
            Log.d("Socket.connected","emitting connect callback");
            sendMessage("connect", messageData);
        }
    };

    private void sendMessage(String messageName,HashMap<String,String> data ) {
        Intent intent = new Intent("SocketIOService");
        data.put("messageName", messageName);
        intent.putExtra("message", data);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
    private void emitMessage(Intent intent){
        try {
            if (intent.hasExtra("message")) {
                HashMap<String, String> message = (HashMap<String, String>) intent.getSerializableExtra("message");
                emitMessage(message);
            } else {
                throw new Exception("No message to handle.");
            }
        } catch(Exception e){
            //TODO handle exception
        }
    }
    private void emitMessage(Message message){
        try {
            Bundle data = message.getData();
            if (data != null) {
                HashMap<String, String> hashMessage = (HashMap<String, String>) data.getSerializable("message");
                emitMessage(hashMessage);
            } else {
                throw new Exception("No data to handle.");
            }
        } catch(Exception e){
            //TODO handle exception
        }
    }
    private void emitMessage(HashMap<String,String> message){
        String eventName = message.remove("event");
        if(eventName != null && !eventName.isEmpty()) {
            mSocket.emit(eventName, new JSONObject(message));
        }
    }

    private JSONObject getJSONData(Object... args){
        JSONObject data = (JSONObject) args[0];
        return data;
    }


/*


* app.io.route('connection',function(client){
    logIn(client.email,client.password);
});
app.io.route('disconnect',function(client){
    logOut(client.id);
});
app.io.route('sign_up',function(client){
    signUp(client.email,client.password);
});
app.io.route('log_in',function(client){
    logIn(client.email,client.password);
});
app.io.route('log_out',function(client){
    logOut(client.id);
});
app.io.route('update_items_list',function(client){
    getItemsList(client.bounds);
});
app.io.route('upload',function(client){
    upload(client.file,client.latLng)
});
app.io.route('download',function(client){
    download(client.id);
});
* */

}
