package com.jnew.draku.membank;

import android.text.TextUtils;

import com.orm.SugarRecord;

/**
 * Created by Michał on 2015-04-05.
 */
 public final class User extends SugarRecord<User> implements IUser {
    private String email;
    private String password;
    private boolean loggedIn;

    public User (){

    }
    public User(String email, String password, boolean loggedIn){
        setEmail(email);
        setPassword(password);
        setLoggedIn(loggedIn);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(User.isValidEmail(email)) {
            this.email = email;
        }
        // TODO Throw error
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
