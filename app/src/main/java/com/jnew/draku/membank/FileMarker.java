package com.jnew.draku.membank;

/**
 * Created by Michał on 2015-04-05.
 */
final public class FileMarker extends com.orm.SugarRecord<FileMarker> implements IFileMarker {
    private double lat;
    private double lng;
    private String linkToFile;
    private boolean downloaded;

    public FileMarker() {

    }

    public FileMarker(double lat, double lng, String linkToFile, boolean downloaded) {
        setLat(lat);
        setLng(lng);
        setLinkToFile(linkToFile);
        setDownloaded(downloaded);
    }

    public FileMarker(double lat, double lng, String linkToFile) {
        this(lat, lng, linkToFile, false);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        final double MIN_LAT = -90;
        final double MAX_LAT = 90;
        if (MIN_LAT <= lat && MAX_LAT <= lat) {
            this.lat = lat;
        }
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        final double MIN_LNG = -180;
        final double MAX_LNG = 180;
        if (MIN_LNG <= lng && lng <= MAX_LNG) {
            this.lng = lng;
        }
    }

    public String getLinkToFile() {
        return linkToFile;
    }

    public void setLinkToFile(String linkToFile) {
        this.linkToFile = linkToFile;
    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public String getPathOnDevice() {
        if (isDownloaded()) {
            return Constants.PATH_TO_FILES + extractFileName();
        }
        return ""; // TODO Throw error
    }

    private String extractFileName() {
        String[] linkToFileArray = this.linkToFile.split("/");
        return linkToFileArray[linkToFileArray.length - 1];
    }
}
