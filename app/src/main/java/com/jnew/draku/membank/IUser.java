package com.jnew.draku.membank;

/**
 * Created by Michał on 2015-04-05.
 */
public interface IUser {
    String getEmail();
     void setEmail(String email);
     String getPassword();
     void setPassword(String password);
     boolean isLoggedIn() ;
     void setLoggedIn(boolean loggedIn);
}
