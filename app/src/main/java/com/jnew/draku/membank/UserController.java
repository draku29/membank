package com.jnew.draku.membank;

import java.util.List;

/**
 * Created by Michał on 2015-04-05.
 */
public class UserController implements IUserController {
    private User user;
    public UserController(){
        setUser(UserController.getLoggedInUser());
    }
    /*public UserController(User user){
        setUser(user);
    }*/
    public UserController(String email, String password, boolean loggedIn){
        User user = new User(email,password,loggedIn);
        this.setUser(user);
        //this.logIn(); // TODO If login fails, redirect to sign up
    }
    public void logIn(){
        User user = this.findUser();
        user.setLoggedIn(true);
        user.save();
    }
    public void logOut(){
        this.user.setLoggedIn(false);
        user.save();
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public void saveNewUser(){
        List<User> users = User.find(User.class,"email = ?",this.user.getEmail());
        if(users.size()==0) {
            this.user.save();
        } else {
            //TODO Throw error
        }

    }
    private User findUser(){
        return UserController.findUser(this.user.getEmail(), this.user.getPassword());
    }
    public static User getLoggedInUser(){
        List<User> users = User.find(User.class,"loggedIn = ?","true");
        if(users.size() > 0) {
            return users.get(0);
        }
        return new User();
        // TODO Throw error

    }
    public static User findUser(String email, String password){
        List<User> users = User.find(User.class,"email = ? and password = ?",email,password);
        if(users.size()>0) {
            return users.get(0);
        }
        return new User();
        //TODO Throw error
    }
}
