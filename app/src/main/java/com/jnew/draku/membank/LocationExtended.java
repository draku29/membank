package com.jnew.draku.membank;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Michał on 2015-03-13.
 */
public class LocationExtended extends Location {
    private final double LOCATION_UNIT = 1.00;
    private LatLngBounds rangeBounds;
    private double longitudeRange = 0 ; // X
    private double latitudeRange = 0; // Y

    public LocationExtended(String provider){
        super(provider);
    }

    public LocationExtended(Location I){
        super(I);
        setRangeBounds();
    }


    public LatLngBounds getRangeBounds() {
        return rangeBounds;
    }
    public void setRangeBounds(){
        LatLng latLngSW = new LatLng(getLatitude() - getLatitudeRange(),getLongitude() - getLongitudeRange());
        LatLng latLngNE = new LatLng(getLatitude() + getLatitudeRange(),getLongitude() + getLongitudeRange());
        rangeBounds = new LatLngBounds(latLngSW,latLngNE);
    }

    public double getLongitudeRange(){
        if(longitudeRange <= 0){
            setLongitudeRange();
        }
        return longitudeRange;
    }
    public double getLatitudeRange(){
        if(latitudeRange <= 0){
            setLatitudeRange();
        }
        return latitudeRange;
    }



    public JSONObject getJSONlatLngBounds(){
        JSONObject mapped = new JSONObject();
        try{
            mapped.put("lat", getLatitude());
            mapped.put("lng", getLongitude());
            mapped.put("latRange",getLatitudeRange());
            mapped.put("lngRange",getLongitudeRange());
            return mapped;
        }catch(JSONException e){
            return null;
        }

    }
    public void setLatitudeRange() {
        this.latitudeRange = Math.abs(calculateLatitudeRange());
    }

    public void setLongitudeRange() {
        this.longitudeRange = Math.abs(calculateLongitudeRange());
    }

    private double calculateLongitudeRange(){
        return getRangeInDegress(newLongitudeSampleLocation());
    }
    private double calculateLatitudeRange(){
        return getRangeInDegress(newLatitudeSampleLocation());
    }
    private double getRangeInDegress(Location sampleLocation){
        final double RANGE = 100.00;
        double distance = distanceTo(sampleLocation);
        return RANGE/distance;
    }
    private Location newLongitudeSampleLocation(){
        Location sampleLocation = new Location("");
        setValidSampleLongitude(sampleLocation);
        return sampleLocation;
    }
    private Location newLatitudeSampleLocation(){
        Location sampleLocation = new Location("");
        setValidSampleLatitude(sampleLocation);
        return sampleLocation;
    }

    private void setValidSampleLatitude(Location sampleLocation){
        double latitude = this.getLatitude();
        if(isLatitudeValid(latitude + LOCATION_UNIT)){
            latitude += LOCATION_UNIT;
        } else {
            latitude -= LOCATION_UNIT;
        }
        sampleLocation.setLatitude(latitude);
    }
    private void setValidSampleLongitude(Location sampleLocation){

        double longitude = this.getLongitude();
        if(isLongitudeValid(longitude + LOCATION_UNIT)){
            longitude += LOCATION_UNIT;
        } else {
            longitude -= LOCATION_UNIT;
        }
        sampleLocation.setLongitude(longitude);
    }
    private boolean isLatitudeValid(double latitude){
        final double MIN_LATITUDE = -90.00;
        final double MAX_LATITUDE = 90.00;
        return (MIN_LATITUDE <= latitude && latitude <= MAX_LATITUDE);
    }
    private boolean isLongitudeValid(double longitude){
        final double MIN_LONGITUDE = -180.00;
        final double MAX_LONGITUDE = 180.00;
        return (MIN_LONGITUDE <= longitude && longitude <= MAX_LONGITUDE );
    }

}
