package com.jnew.draku.membank;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

//TODO !BackgroundLocationService!
final public class BackgroundLocationService extends Service implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener {
    IBinder mBinder = new LocalBinder();
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    // Flag that indicates if a request is underway.
    private boolean mInProgress;
    private boolean servicesAvailable = false;
    private LocationExtended locationWithRange;

    static public final int MSG_GET_LATLNG = 1;
    static public final int MSG_GET_BOUNDS = 2;

    public class LocalBinder extends Binder {
        public BackgroundLocationService getServerInstance() {
            return BackgroundLocationService.this;
        }
    }
    class IncomingHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle data = new Bundle();
            int what = msg.what;
            switch(what){
                case MSG_GET_LATLNG:
                    data.putDouble("lat", locationWithRange.getLatitude() );
                    data.putDouble("lng", locationWithRange.getLongitude());
                    reply(msg.replyTo, what, data);
                    break;
                case MSG_GET_BOUNDS:
                    data.putDouble("lat", locationWithRange.getLatitude() );
                    data.putDouble("lng", locationWithRange.getLongitude());
                    data.putDouble("latRange",locationWithRange.getLatitudeRange());
                    data.putDouble("lngRange",locationWithRange.getLongitudeRange());
                    reply(msg.replyTo, what, data );
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onCreate() {
        super.onCreate();
        mInProgress = false;
// Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
// Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
// Set the update interval to 5 seconds
        mLocationRequest.setInterval(Constants.UPDATE_INTERVAL);
// Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(Constants.FASTEST_INTERVAL);
        servicesAvailable = servicesConnected();
/*
* Create a new location client
*/
        buildGoogleApiClient();
    }
    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }
    private boolean servicesConnected() {
// Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
// If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {

            return true;
        } else {

            return false;
        }
    }
    public int onStartCommand (Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        if(!servicesAvailable || mGoogleApiClient.isConnected() || mInProgress)
            return START_STICKY;
        setUpLocationClientIfNeeded();
        if(!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting() && !mInProgress)
        {
            appendLog(DateFormat.getDateTimeInstance().format(new Date()) + ": Started", Constants.LOG_FILE);
            mInProgress = true;
            mGoogleApiClient.connect();
        }
        return START_STICKY;
    }

    /*
    * Create a new location client, using the enclosing class to
    * handle callbacks.
    */
    private void setUpLocationClientIfNeeded()
    {
        if(mGoogleApiClient == null)
            buildGoogleApiClient();
    }
    // Define the callback method that receives location updates
    @Override
    public void onLocationChanged(Location location) {
        locationWithRange = new LocationExtended(location);
        //TODO Update range and send to UI
// Report to the UI that the location was updated
        String msg = Double.toString(locationWithRange.getLatitude()) + "," +
                Double.toString(locationWithRange.getLongitude());
        Log.d("debug", msg);
// Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        appendLog(msg, Constants.LOCATION_FILE);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    public String getTime() {
        SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return mDateFormat.format(new Date());
    }


    public void appendLog(String text, String filename)
    {
        File logFile = new File(filename);
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
//BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    public void onDestroy(){
// Turn off the request flag
        mInProgress = false;
        if(servicesAvailable && mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);
// Destroy the current location client
            mGoogleApiClient = null;
        }
// Display the connection status
// Toast.makeText(this, DateFormat.getDateTimeInstance().format(new Date()) + ": Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        appendLog(DateFormat.getDateTimeInstance().format(new Date()) + ": Stopped", Constants.LOG_FILE);
        super.onDestroy();
    }
    /*
    * Called by Location Services when the request to connect the
    * client finishes successfully. At this point, you can
    * request the current location or start periodic updates
    */
    @Override
    public void onConnected(Bundle bundle) {
// Request location updates using static settings
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest, this);
        appendLog(DateFormat.getDateTimeInstance().format(new Date()) + ": Connected", Constants.LOG_FILE);

    }

    /*
    * Called by Location Services if the connection to the
    * location client drops because of an error.
    *//*
    @Override
    public void onDisconnected() {
// Turn off the request flag
        mInProgress = false;
// Destroy the current location client
        mGoogleApiClient = null;
// Display the connection status
// Toast.makeText(this, DateFormat.getDateTimeInstance().format(new Date()) + ": Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        appendLog(DateFormat.getDateTimeInstance().format(new Date()) + ": Disconnected", Constants.LOG_FILE);
    }*/

    @Override
    public void onConnectionSuspended(int i) {

    }

    /*
        * Called by Location Services if the attempt to
        * Location Services fails.
        */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mInProgress = false;
/*
* Google Play services can resolve some errors it detects.
* If the error has a resolution, try sending an Intent to
* start a Google Play services activity that can resolve
* error.
*/
        if (connectionResult.hasResolution()) {

// If no resolution is available, display an error dialog
        } else {

        }
    }

    private void reply(Messenger replyTo, int what, Bundle data){
        Message msg = Message.obtain();
        msg.what = what;
        msg.setData(data);
        try {
            replyTo.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}


