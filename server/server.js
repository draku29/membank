app = require('express.io')();
var bodyParser = require('body-parser');
var fs = require('fs');
app.use(bodyParser());
app.http().io();

var mongoose = require('mongoose'),
bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;
mongoose.connect('mongodb://localhost/test');
app.listen(7076);

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var UserSchema = new Schema({
                                email: {type:String, required:true,index:{unique:true}},
                                password: {type:String, required:true},
                                client_id : {type:Array, default:[]}
                            });
UserSchema.pre('save', function(next) { //hashing password
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var MarkerSchema = new Schema({
    //added_by: {type: Schema.Types.ObjectId, required:true, ref: 'User'},
    link_to_file: {type:String, required:true},
    location:{
        lat: {type:Number, required:true, min: -90, max: 90},
        lng: {type:Number, required:true, min: -180, max: 180}
        }
});
var  User = mongoose.model('User',UserSchema);
var  Marker = mongoose.model('Marker',MarkerSchema);
app.get('/download/', function(req, res) {
    var imagepath = req.url.replace("/download/","/image/");
    res.sendFile(req.url);
    //req.io.route('some-cool-realtime-route');
});
app.get('/', function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World\n');
});
app.post('/upload', function(req, res) {
    var image = req.body.image;
    var filename = req.body.filename;
    var latlng = req.body.latLng;

     upload(image,filename,latlng);
});
app.io.route('connection',function(client){
    //logIn(client.email,client.password);
});
app.io.route('disconnect',function(client){
     logOut(client.id);
});
app.io.route('sign_up',function(client){
     signUp(client.email,client.password);
});
app.io.route('log_in',function(client){
     logIn(client.email,client.password);
});
app.io.route('log_out',function(client){
     logOut(client.id);
});
app.io.route('update_items_list',function(client){
    var latLng = client.body.latLng.split(",");
    var lat = latLng[0];
    var lng = latLng[1];
    var minLat = lat - client.body.latRange;
    var maxLat = lat + client.body.latRange;
    var minLng = lng - client.body.lngRange;
    var maxLng = lng + client.body.lngRange;
    var bounds = {
        "minLat":minLat,
        "maxLat":maxLat,
        "minLng":minLng,
        "maxLng":maxLng
    }

     getItemsList(bounds);
});/*
app.io.route('upload',function(client){
    // upload(client.file,client.latLng);
});
app.io.route('download',function(client){
     download(client.id);
});*/


 signUp = function (email,password){
    var query =  User.count({'email': email});
    query.exec(function(err,count){
        if (err) return console.error(err); //TODO Refactor all err returns to throws
        if(count>0){
            console.log('User already exists. Trying to log in.');
             logIn(email,password);
        } else {
            var user = new  User({'email':email,
                                           'password':password});
            user.save(function(err,res){
                if (err) return console.error(err);
                return res;
            });
        }
    });
};
 logIn = function (email,password){
    var query =  User.findOne({'email': email,'password':password});
    query.exec(function(err,user){
       if(err) return console.error(err);
        if(user){
            user.comparePassword(password,function(err,isMatch){
                if(err) return console.error(err);
                return isMatch;
            });
        }
    });
};

 logOut = function (client_id){
    var query =  User.update({'client_id':client_id},{$pull: {'client_id':client_id}});
    query.exec(function(err,res){
        if(err) return console.error(err);
        console.log ('Logged out: '+client_id+' '+res);
    });
};

 upload = function (file, filename, latLng){
    var base64Image = file.toString('base64');
    var decodedImage = new Buffer(base64Image, 'base64');
    var filepath = 'images/'+filename;
    return fs.writeFile(filepath, decodedImage, function(err) {
        if(err) return false;
        var latLngArr = latLng.split(',');
        var lat = latLngArr[0];
        var lng = latLngArr[1];
        var marker = new  Marker({
            'link_to_file' : filename,
            'location':{
                'lat':lat,
                'lng':lng
            }
        });
        marker.save(function(err,res){
            if (err) return console.error(err);
            return res;
        });
        return true;
    });
};
/* download = function (fileId){
    //TODO
    return file;
};*/

 getItemsList = function (latLngBounds){

     Marker.find({"lat":{$gt: latLngBounds.minLat, $lt:latLngBounds.maxLat},"lng":{$gt: latLngBounds.minLng, $lt: latLngBounds.maxLng}},function(err,data){
        if(err) console.log(err); return null;
        return data;
    });

}
